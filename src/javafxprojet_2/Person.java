/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxprojet_2;

import java.time.LocalDate;
import java.time.Period;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

/**
 *
 * @author Malik ibn Zeynab
 */
public class Person {

    private StringProperty firstname, lastename;
    LocalDate birthday;
    private Image photo;

    public Person(String firstname, String lastename, LocalDate birthday) {
        this.firstname = new SimpleStringProperty(firstname);
        this.lastename = new SimpleStringProperty(lastename);
        this.birthday = birthday;
        photo = new Image("default.jpg");
    }

    public Person(String firstname, String lastename, LocalDate birthday, Image photo) {
        this.firstname = new SimpleStringProperty(firstname);
        this.lastename = new SimpleStringProperty(lastename);
        this.birthday = birthday;
        this.photo = photo;
    }

    public final String getFirstname() {
        return firstname.get();
    }

    public void setFirstname(String value) {
        firstname.set(value);
    }

    public String getLastename() {
        return lastename.get();
    }

    public void setLastename(String value) {
        lastename.setValue(value);
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public StringProperty firstnameProperty() {
        return firstname;
    }

    public StringProperty lastnameProperty() {
        return lastename;
    }

    public int getAge() {
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    public Image getImage(){
        return photo;
    }
    
    public void setImage(Image newPicture){
        this.photo= newPicture;
    }
}
