/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxprojet_2;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXRadioButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 *
 * @author Malik ibn Zeynab
 */
public class FXMLProjet_2Controller implements Initializable {
    
    private Label label;
    @FXML
    private Button commander;
    @FXML
    private Label liste;
    @FXML
    private JFXCheckBox burger;
    @FXML
    private JFXCheckBox sandwich;
    @FXML
    private JFXCheckBox chawarma;
    @FXML
    private ChoiceBox<String> choix;
    @FXML
    private JFXRadioButton radioJava;
    @FXML
    private JFXRadioButton radioPHP;
    @FXML
    private JFXRadioButton radioCsharp;
    @FXML
    private JFXRadioButton radionCplus;
    @FXML
    private Label listChoice;
    private ToggleGroup groupRadio;
    @FXML
    private ListView<String> listView;
    @FXML
    private TextArea getTextArea;
    
    @FXML
    public void faireUneCommande() {
        String commande = "Liste commande : ";
        
        if (burger.isSelected()) {
            commande += "\nHumberger";
        }
        if (sandwich.isSelected()) {
            commande += "\nSandwich";
        }
        if (chawarma.isSelected()) {
            commande += "\nChawarma";
        }
        
        liste.setText(commande);
    }
    
    @FXML
    public void changeRadio() {
        if (this.groupRadio.getSelectedToggle().equals(this.radioJava)) {
            listChoice.setText("Vous avez choisi: Java");
        }
        if (this.groupRadio.getSelectedToggle().equals(this.radioPHP)) {
            listChoice.setText("Vous avez choisi: PHP");
        }
        if (this.groupRadio.getSelectedToggle().equals(this.radioCsharp)) {
            listChoice.setText("Vous avez choisi: C#");
        }
        if (this.groupRadio.getSelectedToggle().equals(this.radionCplus)) {
            listChoice.setText("Vous avez choisi: C++");
        }
    }
    
    public void copierListe() {
        String textACopier = "";
        
        ObservableList list = this.listView.getSelectionModel().getSelectedItems();
        
        for (Object item : list) {
            textACopier += String.format("%s%n", (String) item);
        }
        this.getTextArea.setText(textACopier);
    }
    
      public void allerAuComponet2(ActionEvent event) throws IOException{
        Parent ComponentParent = FXMLLoader.load(getClass().getResource("Component2.fxml"));
        Scene componentScene = new Scene(ComponentParent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(componentScene);
        window.show();
    }
      
      
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        liste.setText("");
        choix.getItems().addAll("Mangue", "Banane", "Pomme");
        groupRadio = new ToggleGroup();
        listChoice.setText("");
        
        radioJava.setToggleGroup(groupRadio);
        radioPHP.setToggleGroup(groupRadio);
        radioCsharp.setToggleGroup(groupRadio);
        radionCplus.setToggleGroup(groupRadio);
        
        listView.getItems().addAll("Sonko", "Idy", "Maky", "Issa", "Madické");
        //Accepter la selection multiple
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }
    
}
