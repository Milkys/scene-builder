/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxprojet_2;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Malik ibn Zeynab
 */
public class Component2Controller implements Initializable {

    @FXML
    private TableView<Person> listContact;
    @FXML
    private TableColumn<Person, String> firstname;
    @FXML
    private TableColumn<Person, String> lastname;
    @FXML
    private TableColumn<Person, LocalDate> birthday;
    @FXML
    private JFXTextField nom;
    @FXML
    private JFXTextField prenom;
    @FXML
    private DatePicker anniversiare;
    @FXML
    private Button btnDetailContact;

    public ObservableList<Person> getListe() {
        ObservableList<Person> list = FXCollections.observableArrayList();
        list.add(new Person("Mbengue", "Malick", LocalDate.of(1995, Month.FEBRUARY, 05)));
        list.add(new Person("Ndao", "Aicha", LocalDate.of(2000, Month.MARCH, 02)));
        list.add(new Person("Niang", "Zeynab", LocalDate.of(1960, Month.JULY, 19)));
        return list;
    }

    @FXML
    public void changerNom(CellEditEvent modifier) {
        Person personSelected = listContact.getSelectionModel().getSelectedItem();
        personSelected.setFirstname(modifier.getNewValue().toString());
    }

    @FXML
    public void changerPrenom(CellEditEvent modifier) {
        Person personSelected = listContact.getSelectionModel().getSelectedItem();
        personSelected.setLastename(modifier.getNewValue().toString());
    }

    @FXML
    public void ajouterUnContact() {
        Person newContact = new Person(nom.getText(), prenom.getText(), anniversiare.getValue());
        listContact.getItems().add(newContact);
    }

    @FXML
    public void supprimrContact() {
        ObservableList<Person> selectedRow, allConact;
        //Recuperer tous les personnes de la liste
        allConact = listContact.getItems();

        //Recuperer la ou les lignes selectionnées
        selectedRow = listContact.getSelectionModel().getSelectedItems();

        //Pour chaque personne selectionné...
        selectedRow.forEach((person) -> {
            //Supprimé la personne selectionée au niveau de la liste
            allConact.remove(person);
        });
    }
    
    //Cette methode permet d'activer la bouton Detail du contact si on click sur un contact
    
    public void activerLeBouton(){
        btnDetailContact.setDisable(false);
    }

    @FXML
    public void detailContact(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("DetailContact.fxml"));
        Parent ComponentParent2 = loader.load();
        Scene componentScene = new Scene(ComponentParent2);

        DetailContactController detail = loader.getController();
        detail.initialiserLesDonnees(listContact.getSelectionModel().getSelectedItem());

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(componentScene);
        window.show();

    }

    @FXML
    public void allerAuProjet_2(ActionEvent event) throws IOException {
        Parent ComponentParent = FXMLLoader.load(getClass().getResource("FXMLProjet_2.fxml"));
        Scene componentScene = new Scene(ComponentParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(componentScene);
        window.show();
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        firstname.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        lastname.setCellValueFactory(new PropertyValueFactory<>("lastename"));
        birthday.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        
        listContact.setItems(getListe());
        listContact.setEditable(true);
        firstname.setCellFactory(TextFieldTableCell.forTableColumn());
        lastname.setCellFactory(TextFieldTableCell.forTableColumn());
        listContact.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        //Desactiver le bouton Detaildu contact tant qu'on pas selectionner un contact
        
        btnDetailContact.setDisable(true);

    }

}
