/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxprojet_2;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Malik ibn Zeynab
 */
public class DetailContactController implements Initializable {

    @FXML
    private Label nom;
    @FXML
    private Label prenom;
    @FXML
    private Label age;
    @FXML
    private Label anniversaire;
    private Person contactSelectionne;
    @FXML
    private ImageView photo;
    
    
    public void initialiserLesDonnees(Person person) {
        contactSelectionne = person;
        
        nom.setText(contactSelectionne.getFirstname());
        prenom.setText(contactSelectionne.getLastename());
        age.setText(Integer.toString(contactSelectionne.getAge()));
        anniversaire.setText(contactSelectionne.getBirthday().toString());
        photo.setImage(contactSelectionne.getImage());
        
    }
    
    @FXML
    public void allerAuComponet2(ActionEvent event) throws IOException{
        Parent ComponentParent = FXMLLoader.load(getClass().getResource("Component2.fxml"));
        Scene componentScene = new Scene(ComponentParent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(componentScene);
        window.show();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
